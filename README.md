# Оплата за планых услуг нэска


### Check request:

http://212.42.117.151:8885/payment?command=check&txn_id=15213782&account=1234&sum=0&sum2=574.56&sum3=0.0&sum4=0.0

где ```account``` номер реквизита  
где ```sum2``` сумма оплаты  

```
номер реквизита генерируется мобильным приложением 'Мой свет'
```  

### Check response:  
```xml
<response>
     <txn_id>15213782</txn_id>
     <result>0</result>
     <comment>OK</comment>
     <fields>
          <field1 name="disp1">{"CustomerCode":"339993807","CustomerName":"ОТОГОНОВ","AmountToBePaid":574.56}</field1>
     </fields>
</response>
```

в тег\поле ```result```
```
 0        успешно
 1000	По данному реквизиту платеж уже проведен(платные услуги)
 1001	Реквизит не актуален
``` 
 

в тег\поле ```<field1 name="disp1">``` указаны данные реквизита в виде json:

```json
{
  "CustomerCode": "339993807",
  "CustomerName": "ОТОГОНОВ",
  "AmountToBePaid": 574.56,
}
```

```CustomerCode``` лицевой счет  
```CustomerName``` ФИО  
```AmountToBePaid``` стоимость платной услуги НЭСК  



### Pay request:

http://212.42.117.151:8885/payment?command=pay&txn_id=15213782&txn_date=20090815120133&account=1234&sum=0&sum2=574.56&sum3=0.0&sum4=0.0

где ```account``` номер реквизита  
где ```sum2``` сумма оплаты  

```
номер реквизита генерируется мобильным приложением 'Мой свет'
```  

### Pay response:  
```xml
<?xml version="1.0" encoding="UTF-8"?>
<response>
     <txn_id>15213782</txn_id>
     <prv_txn>null</prv_txn>
     <sum>0.0</sum>
     <sum2>574.56</sum2>
     <sum3>0.0</sum3>
     <sum4>0.0</sum4>
     <result>0</result>
     <comment>OK</comment>
</response>
```


в тег\поле ```<result>``` код возврата:
```
0	ОК
8	Прием платежа запрещен по техническим причинам

400	Прием платежа запрещен провайдером(ээ)
401	Прием платежа запрещен провайдером(платные услуги)
1000	По данному реквизиту платеж уже проведен(платные услуги)
1001	Реквизит не актуален
1002	Не достаточно суммы для проведения платежа(платные услуги)
5000	Внутренняя ошибка
```

